<?php namespace Zoom\Seneka\Models;

use Model;

/**
 * Model
 */
class News extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'zoom_seneka_news';

    public function scopeActive($query){

        return $query->where('is_active', 1)->where('date','<',(new \DateTime("now"))->format("Y-m-d H:i:s"));

    }
}
