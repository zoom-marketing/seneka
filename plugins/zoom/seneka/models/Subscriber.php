<?php namespace Zoom\Seneka\Models;

use Model;

/**
 * Model
 */
class Subscriber extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
        'region' => 'required',
        'email' => 'required|email',
        'city' => 'required|between:2,255',
        'name' => 'required|between:2,255',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'zoom_seneka_subscribers';


    public $belongsTo = [
        'region' => ['Zoom\Seneka\Models\Region'],
    ];
}
