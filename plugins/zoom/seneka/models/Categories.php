<?php namespace Zoom\Seneka\Models;

use Model;

/**
 * Model
 */
class Categories extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'zoom_seneka_categories';

    public $belongsToMany = [
        'lessons' => ['Zoom\Seneka\Models\Lessons', 'table' => 'zoom_seneka_categories_lessons']

    ];

    public $attachOne = [
        'poster' => 'System\Models\File'
    ];


    public function countActiveLessons(){
        $count = 0;
        if($this->lessons){
            $count = $this->lessons->filter(function($value, $key){
                return ($value->is_active && ($value->date >= (new \DateTime('now'))->format("Y-m-d H:i:s")));
            })->count();
        }

        return $count;
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeTypeShowHome($query, $type)
    {
        return $query->where([['type', $type], ['show_home', true]]);
    }


}
