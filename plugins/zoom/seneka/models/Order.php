<?php namespace Zoom\Seneka\Models;

use Model;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */

    public $rules = [
        'lesson' => 'required',
//        'company' => 'required|between:2,64',
        'address' => 'required|between:3,128',
        'city' => 'required|between:2,64',
        'email' => 'required|email',
        'post_code' => 'required|between:5,8',
        'phone' => 'required|between:9,16',
    ];


    public $customMessages = [
//        'company.required' => 'Wymagane jest podanie nazwy firmy.',
        'address.required' => 'Wymagane jest podanie adresu.',
        'address.between' => 'Adres musi mieć od :min do :max znaków.',
        'city.required' => 'Wymagane jest podanie Miasta.',
        'city.between' => 'Miasto musi mieć od :min do :max znaków.',
        'email.required' => 'Wymagane jest podanie adresu e-mail.',
        'email.email' => 'Adresu e-mail ma złą formę.',
        'post_code.required' => 'Wymagane jest podanie kodu pocztowego.',
        'post_code.between' => 'Kod pocztowy musi mieć od :min do :max znaków.',
        'phone.required' => 'Wymagane jest podanie numeru telefonu.',
        'phone.between' => 'Numer telefonu musi mieć od :min do :max znaków.',
        'lesson.required' => 'Szkolenie nie zostało wybrane.',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'zoom_seneka_order';

    protected $jsonable = ['persons'];

    public $belongsTo = [
        'lesson' => ['Zoom\Seneka\Models\Lessons'],
        'source' => ['Zoom\Seneka\Models\Source']
    ];
}
