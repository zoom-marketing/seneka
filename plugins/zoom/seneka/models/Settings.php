<?php namespace Zoom\Seneka\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'zoom_seneka_documents';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

	public $attachOne = [
        'rodo_pdf' => 'System\Models\File',
        'rodo_doc' => 'System\Models\File',
        'application_pdf' => 'System\Models\File',
        'application_doc' => 'System\Models\File',
        'statement_pdf' => 'System\Models\File',
        'statement_doc' => 'System\Models\File',
        'picture_news' => 'System\Models\File',
        'picture_lessons' => 'System\Models\File',
        'picture_company' => 'System\Models\File',
        'picture_company_box' => 'System\Models\File',
	];

    public $attachMany = [
        'picture_home' => 'System\Models\File'
    ];

}