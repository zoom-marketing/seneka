<?php namespace Zoom\Seneka\Models;

use Model;

use Input;
use Request;
use DB;

/**
 * Model
 */
class Lessons extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'zoom_seneka_lessons';


    public $hasMany = [
        'orders' => 'Zoom\Seneka\Models\Order'
    ];

    public $belongsToMany = [
        'categories' => ['Zoom\Seneka\Models\Categories', 'table' => 'zoom_seneka_categories_lessons', 'order' => 'position asc'],
    ];

    public $belongsTo = [
        'teacher' => ['Zoom\Seneka\Models\Teacher', 'table' => 'zoom_seneka_teachers_lessons'],
        'region' => ['Zoom\Seneka\Models\Region'],
        'location' => ['Zoom\Seneka\Models\Location'],
        'program' => ['Zoom\Seneka\Models\Program'],
        'method' => ['Zoom\Seneka\Models\Method'],
        'target' => ['Zoom\Seneka\Models\Target'],
        'target2' => ['Zoom\Seneka\Models\Target'],
        'comment' => ['Zoom\Seneka\Models\Comment'],
        'comment2' => ['Zoom\Seneka\Models\Comment'],
    ];

    public function isAvailable(){
        $lessonDate = new \DateTime($this->date);
        $date = $lessonDate->modify('+1 day');

        return ($this->is_active && $date->format("Y-m-d H:i:s") >= (new \DateTime('now'))->format("Y-m-d H:i:s"));
    }

    public function scopeAvailable($query){
        return $query->where([
            ['is_active', true],
            ['date', '>=', (new \DateTime('now'))->format("Y-m-d H:i:s")],
        ]);
    }

    public function scopeCategory($query, $category_id = 0){

        return $query-> whereHas('categories',function($query) use($category_id) {
            $query-> where('id',$category_id);
        } );

    }

    public function scopeTeacher($query, $teacher_id){

        return $query-> whereHas('teacher',function($query) use($teacher_id) {
            $query-> where('id',$teacher_id);
        } );

    }

    public function scopeSearch($query, $search){

            if(isset($search['categories']) && count($value = array_filter($search['categories'])) > 0){
                  $count = count($search['categories']);
                 // dd($search['categories']);

                  if($count >= 2 && $search['categories'][1] !== '' && $search['categories'][0] !== '') {
                    $productsID = DB::table('zoom_seneka_categories_lessons')->select('lessons_id')->where('categories_id',$search['categories'][0])->get();

                    $result = [];
                    foreach($productsID as $key => $value) {
                        $result[$key] = $value->lessons_id;
                    }
                    $categoryID = $search['categories'][1];
                    $query->whereHas('categories', function($q) use($result, $categoryID) {
                        $q->whereIn('lessons_id', $result);
                        $q->where('categories_id', $categoryID);
                    });
                  }else{
                    $query->whereHas('categories', function($q) use($value) {
                        $q->whereIn('id', $value);
                    });
                }
            }
            if(isset($search['teacher'])){
                $value = $search['teacher'];
                $query->whereHas('teacher', function($q) use($value) {
                    $q->where('id', $value);
                });
            }


            if(isset($search['city'])){
                $value = $search['city'];
                $query->whereHas('location', function($q) use($value) {
                    $q->where('city', '=', $value);
                });
            }

            if(isset($search['date_from']) && isset($search['date_to'])){
               $value =  new \DateTime($search['date_from']);
               $value2 = new \DateTime($search['date_to']);

               $query->whereBetween('date', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date2', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date3', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date4', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date5', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date6', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date7', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date8', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date9', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
               $query->orWhereBetween('date10', [$value->format("Y-m-d H:i:s"), $value2->format("Y-m-d H:i:s")]);
            }else if(isset($search['date_from'])){
                $value =  new \DateTime($search['date_from']);
                $query->where('date', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date2', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date3', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date4', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date5', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date6', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date7', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date8', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date9', '>=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date10', '>=', $value->format("Y-m-d H:i:s"));
            }else if(isset($search['date_to'])){
                $value =  new \DateTime($search['date_to']);
                $query->where('date', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date2', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date3', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date4', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date5', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date6', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date7', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date8', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date9', '<=', $value->format("Y-m-d H:i:s"));
                $query->orWhere('date10', '<=', $value->format("Y-m-d H:i:s"));
            }

            if(isset($search['region'])){

                $value = $search['region'];
                $query->whereHas('region', function($q) use($value) {
                    $q->where('id', $value);
                });
            }
             $query->orderBy('date', 'DESC');
//             dd($query->toSql());
        return $query;
    }

}
