<?php namespace Zoom\Seneka\Classes;


use Zoom\Seneka\Models\Order;
use Renatio\DynamicPDF\Classes\PDF;
use Config;

class PdfOrderService
{
    const FILE_PREFIX = 'order';
    const SALT = 'JFM2eqAU';

    private $order;
    private $storagePath;
    private $cmsStorageUploadsPath;
    private $filePrefix;
    private $templateCode;

    /**
     * PdfOrderService constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->storagePath = storage_path('app/uploads/');
        $this->cmsStorageUploadsPath = url(Config::get('cms.storage.uploads.path'));
        $this->filePrefix = self::FILE_PREFIX;
        $this->templateCode = 'renatio::invoice';
    }

    public function create()
    {
        $pdfFileName = $this->createFileName();
        $pdfFileNameDirectory =  $this->storagePath . $pdfFileName;
        PDF::loadTemplate($this->templateCode, [ 'order' => $this->order ])->setPaper('a4', 'portrait')->save($pdfFileNameDirectory);
        return $baseUrl = $this->cmsStorageUploadsPath . '/' . $pdfFileName;
    }

    private function createFileName():string
    {
        return $this->filePrefix.'-'.$this->createFileId($this->order->id).'.pdf';
    }

    /**
     * @param int $id
     * @return string
     */
    private function createFileId(int $id):string
    {
        return substr(md5($id . self::SALT ), 0, 16);
    }

}
