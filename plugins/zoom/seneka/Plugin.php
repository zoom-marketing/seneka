<?php namespace Zoom\Seneka;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Ustawienia',
                'description' => 'Inne elementy strony',
                'category'    => 'Strona',
                'icon'        => 'icon-cog',
                'class'       => 'Zoom\Seneka\Models\Settings',
                'order'       => 500,
                'keywords'    => 'theme',
                //'permissions' => ['acme.users.access_settings']
            ]
        ];
    }
}
