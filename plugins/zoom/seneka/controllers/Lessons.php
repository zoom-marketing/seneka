<?php namespace Zoom\Seneka\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Zoom\Seneka\Models\Order;
use Zoom\Seneka\Models\Source;

class Lessons extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Zoom.Seneka', 'main-menu-item', 'side-menu-item4');
    }

    public function onImport($lessonId)
    {

        $orderModel  = Order::where('lesson_id', $lessonId)->get();
        $dataCsv[] = [
            'lp',
            'firma',
            'address',
            'nip',
            'miasto',
            'email',
            'kod pocztowy',
            'telefon',
            'fax',
            'komentarz',
            'data utworzenia',
            'data aktualizacji',
            'osoby',
            'Oświadczenie o finansowaniu szkolenia',
            'Skąd dowiedziałeś się o szkoleniu'
        ];

        foreach ($orderModel as $key => $item) {
            $persons = [];
            if($item->attributes['persons']) {
                $personsObj = json_decode($item->attributes['persons']);
                foreach ($personsObj as $key1 => $person) {
                    $persons[$key1] = $person->name . ' [' . $person->workplace . ']';
                }
            }
            $taxRules = '';
            $source = Source::where('id', $item->attributes['source_id'])->first();

            switch($item->attributes['tax_rules'])
            {
                case 1:
                    $taxRules = 'w całości zgodnie z treścią art. 43 ust. 1 pkt 29 lit. c ustawy z dnia 11.03.2004 r. o podatku od towarów i usług (Dz.U. Nr 54, poz. 535 ze zm.)';
                    break;
                case 2:
                    $taxRules =  'w co najmniej 70% zgodnie z treścią § 13 ust. 1 pkt 20 rozporządzenia Ministra Finansów z dnia 22.12.2010 r. w sprawie wykonania niektórych przepisów ustawy o podatku od towarów i usług (Dz. U. Nr 246, poz. 1649)';
                    break;
                default:
                    $taxRules =  'Nie dotyczy';
            }


            $dataCsv[$key+1]['lp'] = $key+1;
            $dataCsv[$key+1]['company'] = $item->attributes['company'];
            $dataCsv[$key+1]['address'] = $item->attributes['address'];
            $dataCsv[$key+1]['tax_no'] = $item->attributes['tax_no'];
            $dataCsv[$key+1]['city'] = $item->attributes['city'];
            $dataCsv[$key+1]['email'] = $item->attributes['email'];
            $dataCsv[$key+1]['post_code'] = $item->attributes['post_code'];
            $dataCsv[$key+1]['phone'] = $item->attributes['phone'];
            $dataCsv[$key+1]['fax'] = $item->attributes['fax'];
            $dataCsv[$key+1]['comments'] = $item->attributes['comments'];
            $dataCsv[$key+1]['created_at'] = $item->attributes['created_at'];
            $dataCsv[$key+1]['updated_at'] = $item->attributes['updated_at'];
            $dataCsv[$key+1]['persons'] = implode(', ', $persons);
            $dataCsv[$key+1]['tax_rules'] = $taxRules;
            $dataCsv[$key+1]['source'] = $source->attributes['name'];

        }

        return response($this->array2csv($dataCsv,';'));

    }

    private function array2csv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\")
    {
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header("Content-Disposition:attachment;filename=osoby.csv");
        $f = fopen('php://memory', 'r+');
        foreach ($data as $item) {
            fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
        }
        rewind($f);
        return stream_get_contents($f);
    }

}
