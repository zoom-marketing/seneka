<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaTeachersLessonsDuration extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
           $table->text('duration')->nullable();
        });
    }

    public function down()
    {
        Schema::table('zoom_seneka_teachers_lessons', function($table)
        {
            $table->text('duration')->nullable();
        });
    }
}
