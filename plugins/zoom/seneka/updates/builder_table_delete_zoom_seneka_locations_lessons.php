<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteZoomSenekaLocationsLessons extends Migration
{
    public function up()
    {
        Schema::dropIfExists('zoom_seneka_locations_lessons');
    }
    
    public function down()
    {
        Schema::create('zoom_seneka_locations_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('lessons_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->primary(['lessons_id','location_id']);
        });
    }
}
