<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaSubscribers extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_subscribers', function($table)
        {
            $table->string('email', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_subscribers', function($table)
        {
            $table->dropColumn('email');
        });
    }
}
