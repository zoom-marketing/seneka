<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons15 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->date('date_to')->nullable();
            $table->dropColumn('city');
            $table->dropColumn('street');
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->dropColumn('date_to');
            $table->string('city', 191)->nullable();
            $table->string('street', 191)->nullable();
        });
    }
}
