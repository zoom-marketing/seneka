<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaTeachersLessons extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_teachers_lessons', function($table)
        {
            $table->dropPrimary(['teachers_id','lessons_id']);
            $table->renameColumn('teachers_id', 'teacher_id');
            $table->primary(['teacher_id','lessons_id']);
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_teachers_lessons', function($table)
        {
            $table->dropPrimary(['teacher_id','lessons_id']);
            $table->renameColumn('teacher_id', 'teachers_id');
            $table->primary(['teachers_id','lessons_id']);
        });
    }
}
