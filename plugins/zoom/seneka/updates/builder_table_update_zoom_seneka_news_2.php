<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaNews2 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_news', function($table)
        {
            $table->dateTime('date')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_news', function($table)
        {
            $table->date('date')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
