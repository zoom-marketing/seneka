<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons22 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->integer('target2_id')->nullable();
            $table->integer('comment2_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->dropColumn('target2_id');
            $table->dropColumn('comment2_id');
        });
    }
}
