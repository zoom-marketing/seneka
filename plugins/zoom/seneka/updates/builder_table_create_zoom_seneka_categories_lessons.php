<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaCategoriesLessons extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_categories_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('categories_id')->unsigned();
            $table->integer('lessons_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_categories_lessons');
    }
}
