<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaOrder7 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->renameColumn('rules', 'tax_rules');
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->renameColumn('tax_rules', 'rules');
        });
    }
}
