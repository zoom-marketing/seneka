<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaLessons extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('category')->nullable();
            $table->string('title')->nullable();
            $table->date('date')->nullable();
            $table->string('city')->nullable();
            $table->string('teacher')->nullable();
            $table->integer('price')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_lessons');
    }
}
