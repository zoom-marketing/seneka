<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaTeachers extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_teachers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64)->nullable();
            $table->string('position', 64)->nullable();
            $table->string('content', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_teachers');
    }
}
