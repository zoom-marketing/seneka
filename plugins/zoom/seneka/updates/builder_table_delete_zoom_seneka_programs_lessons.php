<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteZoomSenekaProgramsLessons extends Migration
{
    public function up()
    {
        Schema::dropIfExists('zoom_seneka_programs_lessons');
    }
    
    public function down()
    {
        Schema::create('zoom_seneka_programs_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('lessons_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->primary(['lessons_id','program_id']);
        });
    }
}
