<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategories4 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('type')->nullable(false)->default(0)->change();
            $table->boolean('show_home')->nullable(false)->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('type')->nullable()->default(null)->change();
            $table->boolean('show_home')->nullable()->default(null)->change();
        });
    }
}
