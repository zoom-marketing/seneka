<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessonsHours extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->string('hours')->nullable()->unsigned(false)->default(null);
        });
    }

    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->string('hours')->nullable()->unsigned(false)->default(null);
        });
    }
}
