<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategories extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('type');
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->dropColumn('type');
        });
    }
}
