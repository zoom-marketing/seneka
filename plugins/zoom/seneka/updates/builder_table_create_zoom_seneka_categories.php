<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaCategories extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_categories');
    }
}
