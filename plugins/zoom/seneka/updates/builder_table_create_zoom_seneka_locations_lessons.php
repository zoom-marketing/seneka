<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaLocationsLessons extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_locations_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('lessons_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->primary(['lessons_id','location_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_locations_lessons');
    }
}
