<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteZoomSenekaNumbers extends Migration
{
    public function up()
    {
        Schema::dropIfExists('zoom_seneka_numbers');
    }
    
    public function down()
    {
        Schema::create('zoom_seneka_numbers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('value');
            $table->string('text', 191);
        });
    }
}
