<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategoriesIsOnline extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('is_online')->nullable(false)->default(0);
        });
    }

    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('is_online')->nullable(false)->default(0);
        });
    }
}
