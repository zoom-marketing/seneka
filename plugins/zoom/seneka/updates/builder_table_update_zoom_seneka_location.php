<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLocation extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_location', function($table)
        {
            $table->string('city', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_location', function($table)
        {
            $table->dropColumn('city');
        });
    }
}
