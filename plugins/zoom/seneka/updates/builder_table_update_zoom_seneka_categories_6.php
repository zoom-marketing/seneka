<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategories6 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->integer('position')->nullable(false)->default(0);
        });
    }

    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->integer('position')->nullable(false)->default(0);
        });
    }
}
