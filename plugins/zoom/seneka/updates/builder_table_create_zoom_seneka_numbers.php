<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaNumbers extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_numbers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('value');
            $table->string('text');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_numbers');
    }
}
