<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaPrograms extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_programs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('description')->nullable();
            $table->string('name', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_programs');
    }
}
