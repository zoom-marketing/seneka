<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaSource extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_source', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_source');
    }
}
