<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessonOnline extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
           $table->boolean('online')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_teachers_lessons', function($table)
        {
           $table->boolean('online')->nullable();
        });
    }
}
