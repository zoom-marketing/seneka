<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaRegions extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_regions', function($table)
        {
            $table->string('name', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_regions', function($table)
        {
            $table->string('name', 191)->nullable(false)->change();
        });
    }
}
