<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaOrder6 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->smallInteger('rules')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->dropColumn('rules');
        });
    }
}
