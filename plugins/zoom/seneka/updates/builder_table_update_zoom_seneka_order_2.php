<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaOrder2 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->text('persons')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->dropColumn('persons');
        });
    }
}
