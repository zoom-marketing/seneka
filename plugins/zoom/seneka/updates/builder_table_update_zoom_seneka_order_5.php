<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaOrder5 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->integer('source_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_order', function($table)
        {
            $table->dropColumn('source_id');
        });
    }
}
