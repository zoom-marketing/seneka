<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons7 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->renameColumn('teacher', 'teacher_id');
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->renameColumn('teacher_id', 'teacher');
        });
    }
}
