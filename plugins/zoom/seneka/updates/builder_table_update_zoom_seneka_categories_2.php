<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategories2 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->text('description')->nullable();
            $table->string('title', 191)->nullable()->change();
            $table->boolean('type')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->dropColumn('description');
            $table->string('title', 191)->nullable(false)->change();
            $table->boolean('type')->nullable(false)->change();
        });
    }
}
