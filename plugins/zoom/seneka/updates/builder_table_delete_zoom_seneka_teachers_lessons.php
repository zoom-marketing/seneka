<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteZoomSenekaTeachersLessons extends Migration
{
    public function up()
    {
        Schema::dropIfExists('zoom_seneka_teachers_lessons');
    }
    
    public function down()
    {
        Schema::create('zoom_seneka_teachers_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('teacher_id')->unsigned();
            $table->integer('lessons_id')->unsigned();
            $table->primary(['teacher_id','lessons_id']);
        });
    }
}
