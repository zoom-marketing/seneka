<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons2 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->string('street')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->dropColumn('street');
        });
    }
}
