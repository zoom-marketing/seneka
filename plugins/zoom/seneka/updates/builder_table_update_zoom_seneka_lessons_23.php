<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons23 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->dateTime('date')->nullable()->unsigned(false)->default(null)->change();
            $table->dateTime('date_to')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->date('date')->nullable()->unsigned(false)->default(null)->change();
            $table->date('date_to')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
