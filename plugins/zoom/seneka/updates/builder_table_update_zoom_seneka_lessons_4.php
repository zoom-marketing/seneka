<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaLessons4 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->text('program')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
            $table->dropColumn('program');
        });
    }
}
