<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaTeachersLessons extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_teachers_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('teachers_id')->unsigned();
            $table->integer('lessons_id')->unsigned();
            $table->primary(['teachers_id','lessons_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_teachers_lessons');
    }
}
