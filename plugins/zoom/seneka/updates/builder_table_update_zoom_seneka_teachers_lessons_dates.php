<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaTeachersLessonsDates extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_lessons', function($table)
        {
           $table->dateTime('date2')->nullable();
           $table->dateTime('date_to2')->nullable();
           $table->dateTime('date3')->nullable();
           $table->dateTime('date_to3')->nullable();
           $table->dateTime('date4')->nullable();
           $table->dateTime('date_to4')->nullable();
           $table->dateTime('date5')->nullable();
           $table->dateTime('date_to5')->nullable();
           $table->dateTime('date6')->nullable();
           $table->dateTime('date_to6')->nullable();
           $table->dateTime('date7')->nullable();
           $table->dateTime('date_to7')->nullable();
           $table->dateTime('date8')->nullable();
           $table->dateTime('date_to8')->nullable();
           $table->dateTime('date9')->nullable();
           $table->dateTime('date_to9')->nullable();
           $table->dateTime('date10')->nullable();
           $table->dateTime('date_to10')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_teachers_lessons', function($table)
        {
           $table->dateTime('date2')->nullable();
           $table->dateTime('date_to2')->nullable();
           $table->dateTime('date3')->nullable();
           $table->dateTime('date_to3')->nullable();
           $table->dateTime('date4')->nullable();
           $table->dateTime('date_to4')->nullable();
           $table->dateTime('date5')->nullable();
           $table->dateTime('date_to5')->nullable();
           $table->dateTime('date6')->nullable();
           $table->dateTime('date_to6')->nullable();
           $table->dateTime('date7')->nullable();
           $table->dateTime('date_to7')->nullable();
           $table->dateTime('date8')->nullable();
           $table->dateTime('date_to8')->nullable();
           $table->dateTime('date9')->nullable();
           $table->dateTime('date_to9')->nullable();
           $table->dateTime('date10')->nullable();
           $table->dateTime('date_to10')->nullable();
        });
    }
}
