<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategories3 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->boolean('show_home')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_categories', function($table)
        {
            $table->dropColumn('show_home');
        });
    }
}
