<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateZoomSenekaOrder extends Migration
{
    public function up()
    {
        Schema::create('zoom_seneka_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('company', 64)->nullable();
            $table->string('address', 128)->nullable();
            $table->string('tax_no', 16)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('email', 128)->nullable();
            $table->string('post_code', 8)->nullable();
            $table->string('phone', 16)->nullable();
            $table->string('fax', 16)->nullable();
            $table->string('comments', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('zoom_seneka_order');
    }
}
