<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaNews3 extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_news', function($table)
        {
            $table->string('title', 255)->change();
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_news', function($table)
        {
            $table->string('title', 64)->change();
        });
    }
}
