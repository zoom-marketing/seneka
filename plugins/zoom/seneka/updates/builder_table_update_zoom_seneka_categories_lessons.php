<?php namespace Zoom\Seneka\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateZoomSenekaCategoriesLessons extends Migration
{
    public function up()
    {
        Schema::table('zoom_seneka_categories_lessons', function($table)
        {
            $table->primary(['categories_id','lessons_id']);
        });
    }
    
    public function down()
    {
        Schema::table('zoom_seneka_categories_lessons', function($table)
        {
            $table->dropPrimary(['categories_id','lessons_id']);
        });
    }
}
