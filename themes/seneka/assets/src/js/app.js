window.$ = window.jQuery = require('jquery');
require('bootstrap');
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
import './countTo';
import './clear_form'

var formPerson = {
  init: function() {
    $('#addPerson').on('click', function (event) {
      event.preventDefault();
      formPerson.add();
    });
    this.refreshElements();
  },

  refreshElements: function(){
    // language=JQuery-CSS
    $('.js-person-remove').click(function (event) {
      event.preventDefault();
      let personId = $(event.target).data('person-id');
      formPerson.remove(personId);
    });
  },

  add: function () {
    let list = $('#personList');
    let personId = Math.floor((Math.random() * 100000) + 1);
    console.log(personId);
    list.append('<div class="js-person-row form__row" data-person-id="' + personId + '">\n' +
      '                <div class="form__col">\n' +
      '                    <label class="contact-form__label">Imię i nazwisko: </label><br>\n' +
      '                    <input maxlength="64" name="persons[' + personId + '][name]" class="contact-form__input" required>\n' +
      '                </div>\n' +
      '                <div class="form__col">\n' +
      '                    <label class="contact-form__label">Stanowisko: </label><br>\n' +
      '                    <input maxlength="32" name="persons[' + personId + '][workplace]" class="contact-form__input">\n' +
      '                </div>\n' +
      '                <div class="form__remove"><div data-person-id="' + personId + '" class="js-person-remove form__remove-button">Usuń</div></div>\n' +
      '           </div>').hide().fadeIn(750);
    this.refreshElements();
  },

  remove: function (personId) {
    $('.js-person-row[data-person-id="' + personId + '"]').fadeOut(750, function () {
      $(this).remove();
    });
  }


};

$(document).ready(function(){
      $('.js-photo').owlCarousel({
        loop:true,
        nav: true,
        dots: false,
        navText: ["<i class='fas fa-2x fa-arrow-circle-left'></i>","<i class='fas fa-2x fa-arrow-circle-right'></i>"],
        margin:0,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                dots: true,
                dotsEach: 2,
            },
            576:{
                items:2,
                dots: true,
                dotsEach: 2,
            },
            960:{
                items:3,
                dots: true,
                dotsEach: 3,
            },
            1140:{
                items:3,
                dotsEach: 3,
            },
            1335:{
                items:4,
                dotsEach: 4,
            }
        }
    });


    $('.js-news').owlCarousel({
      loop:true,
      margin:0,
      responsiveClass:true,
      responsive:{
          0:{
            items:1,
            dots: true,
            dotsEach: 2,
          },
          576:{
            items:2,
            dots: true,
            dotsEach: 2,
          },
          960:{
            items:4,
            dots: true,
            dotsEach: 4,
          },
          1140:{
            items:4,
            dotsEach: 4,
          }
      }
    });
    $('.count').countTo();

    formPerson.init();
});


var startPosition = null;

$(window).scroll(function() {
	    let scroll = $(window).scrollTop();
	    // let WindowHeight = jQuery(window).height();
      let el = $("#nav");
	    if (scroll > 110) {

        if(el.hasClass("nav-color--dark")) {
          $("#nav").removeClass("nav-color--dark").addClass("nav-color--light-scroll");
        }
	    }
	    else{
	      if(el.hasClass("nav-color--light-scroll")){
          el.removeClass("nav-color--light-scroll").addClass("nav-color--dark");
        }
	    }

	    //Order button
	    let orderScroll = function () {


        const navHeight = $('#nav').height();

        let currentTop = $(window).scrollTop();
        let element = $('#order-button');

        if(element.length > 0) {

          if(startPosition == null){
            startPosition = { top: 0, left: 0, right: 0};
            startPosition.top = element.offset().top;
            startPosition.left = element.offset().left;
            startPosition.right = element.offset().right;
            // startPosition.width = element.innerWidth();

          }

          if((currentTop + navHeight) >= startPosition.top){
            element.css("position",  "fixed");
            if($(window).width() < 768){
              element.css("top",  "auto");
              element.css("bottom",  0);
              element.css("left",  0);
              element.css("right",  0);
              element.css("z-index",  9999);
            }else
            {
              element.css("top",  navHeight);
              element.css("bottom",  "auto");
              element.css("left",  startPosition.left);
              element.css("right",  startPosition.right);
              element.css("z-index",  9999);
            }


          }
          else {
            element.css("position",  "relative");
            element.css("top",  "auto");
            element.css("bottom",  "auto");
            element.css("left",  "auto");
            element.css("right",  "auto");

            startPosition = { top: 0, left: 0, right: 0};
            startPosition.top = element.offset().top;
            startPosition.left = element.offset().left;
            startPosition.right = element.offset().right;
          }

        }
      };
	    orderScroll();

});

