const clearForm = document.querySelector(".search__form--clear");

if (clearForm) {
    const searchInputs = document.querySelectorAll(".search__form--choose");
    clearForm.addEventListener('click', function () {
        searchInputs.forEach(input => {
            input.value = "";
        })
    })
}
